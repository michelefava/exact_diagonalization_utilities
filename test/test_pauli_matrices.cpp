#include "catch.hpp"

#include "../includes/product_state.hpp"
#include "../includes/basis.hpp"
#include "../includes/pauli_operators.hpp"

#include <armadillo>
#include <iostream>

TEST_CASE("Pauli matrices identities, site 1","[Pauli matricies]"){
    const int L = 10;
    const int d = 2;
    const int site = 1;
    const double tolerance = 1e-10;

    auto base = std::make_shared<sp_spin_1_2_basis>(sp_spin_1_2_basis(L));

    auto sx = to_complex(sigmax(site, base));
    auto sy = sigmay(site, base);
    auto sz = to_complex(sigmaz(site, base));
    auto sp = to_complex(sigma_plus(site, base));
    auto sm = to_complex(sigma_minus(site, base));
    auto id = to_complex(identity(base));

    SECTION("Sx*Sy = -Sy*Sx"){
        REQUIRE(arma::norm((sx*sy+sy*sx).mat)< tolerance);
    }

    SECTION("Sx*Sz = -Sz*Sx"){
        REQUIRE(arma::norm((sx*sz+sz*sx).mat)< tolerance);
    }

    SECTION("Sx*Sz = -Sz*Sx"){
        REQUIRE(arma::norm((sx*sz+sz*sx).mat)< tolerance);
    }

    SECTION("Sx^2 = id"){
        REQUIRE(arma::norm((sx*sx-id).mat)< tolerance);
    }

    SECTION("Sy^2 = id"){
        REQUIRE(arma::norm((sy*sy-id).mat)< tolerance);
    }

    SECTION("Sz^2 = id"){
        REQUIRE(arma::norm((sz*sz-id).mat)< tolerance);
    }

    Complex im {0, 1};

    SECTION("Sx*Sy= i Sz"){
        REQUIRE(arma::norm((sx*sy-im*sz).mat)< tolerance);
    }

    SECTION("Sx*Sz= - i Sy"){
        REQUIRE(arma::norm((sx*sz+im*sy).mat)< tolerance);
    }

    SECTION("Sy*Sz=  i Sx"){
        REQUIRE(arma::norm((sy*sz-im*sx).mat)< tolerance);
    }

    SECTION("Sp*Sm = (Sz + 1)/2"){
        REQUIRE(arma::norm((sp*sm-0.5*(sz+id)).mat)< tolerance);
    }
}

TEST_CASE("Pauli matrices identities, site 5","[Pauli matricies]"){
    const int L = 10;
    const int d = 2;
    const int site = 5;
    const double tolerance = 1e-10;

    auto base = std::make_shared<sp_spin_1_2_basis>(sp_spin_1_2_basis(L));

    auto sx = to_complex(sigmax(site, base));
    auto sy = sigmay(site, base);
    auto sz = to_complex(sigmaz(site, base));
    auto sp = to_complex(sigma_plus(site, base));
    auto sm = to_complex(sigma_minus(site, base));
    auto id = to_complex(identity(base));

    SECTION("Sx*Sy = -Sy*Sx"){
        REQUIRE(arma::norm((sx*sy+sy*sx).mat)< tolerance);
    }

    SECTION("Sx*Sz = -Sz*Sx"){
        REQUIRE(arma::norm((sx*sz+sz*sx).mat)< tolerance);
    }

    SECTION("Sx*Sz = -Sz*Sx"){
        REQUIRE(arma::norm((sx*sz+sz*sx).mat)< tolerance);
    }

    SECTION("Sx^2 = id"){
        REQUIRE(arma::norm((sx*sx-id).mat)< tolerance);
    }

    SECTION("Sy^2 = id"){
        REQUIRE(arma::norm((sy*sy-id).mat)< tolerance);
    }

    SECTION("Sz^2 = id"){
        REQUIRE(arma::norm((sz*sz-id).mat)< tolerance);
    }

    Complex im {0, 1};

    SECTION("Sx*Sy= i Sz"){
        REQUIRE(arma::norm((sx*sy-im*sz).mat)< tolerance);
    }

    SECTION("Sx*Sz= - i Sy"){
        REQUIRE(arma::norm((sx*sz+im*sy).mat)< tolerance);
    }

    SECTION("Sy*Sz=  i Sx"){
        REQUIRE(arma::norm((sy*sz-im*sx).mat)< tolerance);
    }

    SECTION("Sp*Sm = (Sz + 1)/2"){
        REQUIRE(arma::norm((sp*sm-0.5*(sz+id)).mat)< tolerance);
    }
}

TEST_CASE("Pauli matrices identities, site 9","[Pauli matricies]"){
    const int L = 9;
    const int d = 2;
    const int site = 5;
    const double tolerance = 1e-10;

    auto base = std::make_shared<sp_spin_1_2_basis>(sp_spin_1_2_basis(L));

    auto sx = to_complex(sigmax(site, base));
    auto sy = sigmay(site, base);
    auto sz = to_complex(sigmaz(site, base));
    auto sp = to_complex(sigma_plus(site, base));
    auto sm = to_complex(sigma_minus(site, base));
    auto id = to_complex(identity(base));

    SECTION("Sx*Sy = -Sy*Sx"){
        REQUIRE(arma::norm((sx*sy+sy*sx).mat)< tolerance);
    }

    SECTION("Sx*Sz = -Sz*Sx"){
        REQUIRE(arma::norm((sx*sz+sz*sx).mat)< tolerance);
    }

    SECTION("Sx*Sz = -Sz*Sx"){
        REQUIRE(arma::norm((sx*sz+sz*sx).mat)< tolerance);
    }

    SECTION("Sx^2 = id"){
        REQUIRE(arma::norm((sx*sx-id).mat)< tolerance);
    }

    SECTION("Sy^2 = id"){
        REQUIRE(arma::norm((sy*sy-id).mat)< tolerance);
    }

    SECTION("Sz^2 = id"){
        REQUIRE(arma::norm((sz*sz-id).mat)< tolerance);
    }

    Complex im {0, 1};

    SECTION("Sx*Sy= i Sz"){
        REQUIRE(arma::norm((sx*sy-im*sz).mat)< tolerance);
    }

    SECTION("Sx*Sz= - i Sy"){
        REQUIRE(arma::norm((sx*sz+im*sy).mat)< tolerance);
    }

    SECTION("Sy*Sz=  i Sx"){
        REQUIRE(arma::norm((sy*sz-im*sx).mat)< tolerance);
    }

    SECTION("Sp*Sm = (Sz + 1)/2"){
        REQUIRE(arma::norm((sp*sm-0.5*(sz+id)).mat)< tolerance);
    }
}