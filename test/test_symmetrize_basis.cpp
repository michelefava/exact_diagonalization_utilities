#include "catch.hpp"

#include "../includes/basis_build.hpp"
#include "../includes/translated_state.hpp"
#include "../includes/reflected_translated_state.hpp"

TEST_CASE("symmetrized_basis out of translated_states","[basis]"){
    SECTION("4 sites"){
        int L = 4;

        sp_spin_1_2_basis spin_basis = sp_spin_1_2_basis(L);

        const int d = 2;

        dense_basis<translated_state> trans_basis = dense_basis<translated_state>(L,d,pow(2,L)/L*2);

        symmetrize_basis(spin_basis, trans_basis);

        //check correct dimension
        REQUIRE(trans_basis.dim() == 6);

        //check all states are differen
        int D = trans_basis.dim();
        bool flag = true;
        for (int i=0; i<D; i++){
            for (int j=i+1; j<D; j++){
                if (trans_basis[i]==trans_basis[j]){
                    flag = false;
                }
            }
        }
        REQUIRE(flag);
    }

    SECTION("5 sites"){
        int L = 5;

        sp_spin_1_2_basis spin_basis = sp_spin_1_2_basis(L);

        const int d = 2;

        dense_basis<translated_state> trans_basis = dense_basis<translated_state>(L,d,pow(2,L)/L*2);

        symmetrize_basis(spin_basis, trans_basis);

        //check correct dimension
        for (int i=0; i<trans_basis.dim(); i++){
            std::cout << trans_basis[i].representative() <<"\n";
        }
        REQUIRE(trans_basis.dim() == 8);

        //check all states are differen
        int D = trans_basis.dim();
        bool flag = true;
        for (int i=0; i<D; i++){
            for (int j=i+1; j<D; j++){
                if (trans_basis[i]==trans_basis[j]){
                    flag = false;
                }
            }
        }
        REQUIRE(flag);
    }

    SECTION("6 sites"){
        int L = 6;

        sp_spin_1_2_basis spin_basis = sp_spin_1_2_basis(L);

        const int d = 2;

        dense_basis<translated_state> trans_basis = dense_basis<translated_state>(L,d,pow(2,L)/L*2);

        symmetrize_basis(spin_basis, trans_basis);

        //check correct dimension
        REQUIRE(trans_basis.dim() == 14);

        //check all states are differen
        int D = trans_basis.dim();
        bool flag = true;
        for (int i=0; i<D; i++){
            for (int j=i+1; j<D; j++){
                if (trans_basis[i]==trans_basis[j]){
                    flag = false;
                }
            }
        }
        REQUIRE(flag);
    }



}

TEST_CASE("symmetrized_basis out of reflected_translated_states","[basis]"){

    SECTION("4 sites"){

        int L = 4;

        sp_spin_1_2_basis spin_basis = sp_spin_1_2_basis(L);

        const int d = 2;

        dense_basis<reflected_translated_state> trans_basis = dense_basis<reflected_translated_state>(L,d,pow(2,L)/L);

        symmetrize_basis(spin_basis, trans_basis);

        //check correct dimension
        REQUIRE(trans_basis.dim() == 6);

        //check all states are differen
        int D = trans_basis.dim();
        bool flag = true;
        for (int i=0; i<D; i++){
            for (int j=i+1; j<D; j++){
                if (trans_basis[i]==trans_basis[j]){
                    flag = false;
                }
            }
        }
        REQUIRE(flag);

    }

    SECTION("5 sites"){

        int L = 5;

        sp_spin_1_2_basis spin_basis = sp_spin_1_2_basis(L);

        const int d = 2;

        dense_basis<reflected_translated_state> trans_basis = dense_basis<reflected_translated_state>(L,d,pow(2,L)/L);

        symmetrize_basis(spin_basis, trans_basis);

        //check correct dimension
        REQUIRE(trans_basis.dim() == 8);

        //check all states are differen
        int D = trans_basis.dim();
        bool flag = true;
        for (int i=0; i<D; i++){
            for (int j=i+1; j<D; j++){
                if (trans_basis[i]==trans_basis[j]){
                    flag = false;
                }
            }
        }
        REQUIRE(flag);
    }

    SECTION("6 sites (some non-even state)"){

        int L = 6;

        sp_spin_1_2_basis spin_basis = sp_spin_1_2_basis(L);

        const int d = 2;

        dense_basis<reflected_translated_state> trans_basis = dense_basis<reflected_translated_state>(L,d,pow(2,L)/L);

        symmetrize_basis(spin_basis, trans_basis);

            //check correct dimension
        REQUIRE(trans_basis.dim() == 13);

        //check all states are differen
        int D = trans_basis.dim();
        bool flag = true;
        for (int i=0; i<D; i++){
            for (int j=i+1; j<D; j++){
                if (trans_basis[i]==trans_basis[j]){
                    flag = false;
                }
            }
        }
        REQUIRE(flag);
    }
    
}