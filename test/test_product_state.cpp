#include "catch.hpp"

#include "../includes/symmetries_implementation.hpp"
#include "../includes/product_state.hpp"



TEST_CASE("operations on pruduct states", "[product_state]")
{
    int L = 5;
    int d = 2;
    product_state s = product_state(L, d);
    s.set_site(0, 0);
    s.set_site(1, 0);
    s.set_site(2, 0);
    s.set_site(3, 0);
    s.set_site(4, 0);

    SECTION("writing on non-existent site"){
        REQUIRE_THROWS_AS(s.set_site(5,1), product_state::invalid_site);
        REQUIRE_THROWS_AS(s.set_site(-1, 1), product_state::invalid_site);
    }

    SECTION("reading non-existent site"){
        REQUIRE_THROWS_AS(s.get_site(5), product_state::invalid_site);
        REQUIRE_THROWS_AS(s.get_site(-1), product_state::invalid_site);
    }

    SECTION("introducing non-existent state"){
        REQUIRE_THROWS_AS(s.set_site(2, -1), product_state::invalid_state);
        REQUIRE_THROWS_AS(s.set_site(2, 2), product_state::invalid_state);
    }

    SECTION("== working"){
        auto sp = product_state(s.L(),s.d());
        for (int i=0; i<s.L(); i++)
            sp.set_site(i,s.get_site(i));
        
        REQUIRE(sp == s);

        REQUIRE_FALSE(sp != s);
    }

    SECTION("== failing"){
        auto sp = product_state(s.L(), s.d());
        for (int i = 0; i < s.L(); i++)
            sp.set_site(i, s.get_site(i));
        
        sp.set_site(4,!s.get_site(4));
        REQUIRE_FALSE(sp == s);
        REQUIRE(sp != s);
    }

    SECTION("== with different L"){
        auto sp = product_state(s.L()+1, s.d());
        REQUIRE_THROWS_AS(sp == s, mismatching_system_size);

        sp = product_state(s.L() - 1, s.d());
        REQUIRE_THROWS_AS(sp == s, mismatching_system_size);
    }

    SECTION("== with different d"){
        auto sp = product_state(s.L(), s.d()-1);
        REQUIRE_THROWS_AS(sp == s, mismatching_local_dimensions);

        sp = product_state(s.L(), s.d()+1);
        REQUIRE_THROWS_AS(sp == s, mismatching_local_dimensions);
    }
}

TEST_CASE("vector assignment","[product state]"){
    int L = 5;
    int d = 2;
    product_state s = product_state(L, d);
    s.set_configuration({0, 1, 0, 1, 1});

    product_state sp = product_state(5, 2);
    sp.set_site(0,0);
    sp.set_site(1, 1);
    sp.set_site(2, 0);
    sp.set_site(3, 1);
    sp.set_site(4, 1);

    REQUIRE(s == sp);
}

TEST_CASE("shift_to_right and reflect pruduct states (odd length)", "[product_state]"){
    int L = 5;
    int d = 2;
    product_state s = product_state(L, d);
    s.set_configuration({0, 1, 0, 1, 1});

    SECTION("shift_right"){
        shift_state_to_right(s);
        product_state sp = product_state(5, 2);
        sp.set_configuration({1, 0, 1, 0, 1});
        REQUIRE(sp == s);
    }

    SECTION("reflect")
    {
        reflect_state(s);
        product_state sp = product_state(5, 2);
        sp.set_configuration({1, 1, 0, 1, 0});
        REQUIRE(sp == s);
    }
}

TEST_CASE("shift_to_right and reflect product states (even length)", "[product_state]")
{
    int L = 6;
    int d = 2;
    product_state s = product_state(L, d);
    s.set_configuration({1, 1, 0, 1, 0, 1});

    SECTION("shift_right")
    {
        shift_state_to_right(s);
        product_state sp = product_state(6, 2);
        sp.set_configuration({1, 1, 1, 0, 1, 0});
        REQUIRE(sp == s);
    }

    SECTION("reflect")
    {   
        reflect_state(s);
        product_state sp = product_state(6, 2);
        sp.set_configuration({1, 0, 1, 0, 1, 1});
        REQUIRE(sp == s);
    }
}