#include "catch.hpp"

#include "../includes/translated_state.hpp"
#include "../includes/product_state.hpp"
#include "../includes/reflected_translated_state.hpp"

#include "../includes/basis.hpp"


TEST_CASE("correct period, odd sites", "[translated_state]")
{
    const int L = 9;
    const int d = 2;
    product_state s = product_state(L,d);

    SECTION("period 1"){

        s.set_configuration({1, 1, 1, 1, 1, 1, 1, 1, 1});

        auto ss = translated_state(s);

        REQUIRE(ss.translation_period()==1);

    }

    SECTION("period L"){

        s.set_configuration({1, 0, 1, 0, 1, 0, 1, 1, 1});

        auto ss = translated_state(s);

        REQUIRE(ss.translation_period()==L);

    }

    SECTION("period 3"){

        s.set_configuration({1, 0, 0, 1, 0, 0, 1, 0, 0});

        auto ss = translated_state(s);

        REQUIRE(ss.translation_period()==3);

    }

    SECTION("period L again"){

        s.set_configuration({1, 0, 0, 1, 0, 0, 1, 0, 1});

        auto ss = translated_state(s);

        REQUIRE(ss.translation_period()==L);

    }

    SECTION("period L once more"){

        s.set_configuration({1, 0, 1, 0, 1, 0, 1, 0, 1});

        auto ss = translated_state(s);

        REQUIRE(ss.translation_period()==L);

    }
}

TEST_CASE("correct period, even sites", "[translated_state]")
{
    const int L = 8;
    const int d = 2;
    product_state s = product_state(L,d);

    SECTION("period 1"){

        s.set_configuration({1, 1, 1, 1, 1, 1, 1, 1});

        auto ss = translated_state(s);

        REQUIRE(ss.translation_period()==1);

    }

    SECTION("period L"){

        s.set_configuration({1, 0, 1, 0, 1, 0, 1, 1});

        auto ss = translated_state(s);

        REQUIRE(ss.translation_period()==L);

    }

    SECTION("period 2"){

        s.set_configuration({1, 0, 1, 0, 1, 0, 1, 0});

        auto ss = translated_state(s);

        REQUIRE(ss.translation_period()==2);

    }

    SECTION("period L again"){

        s.set_configuration({1, 0, 0, 1, 0, 0, 1, 0});

        auto ss = translated_state(s);

        REQUIRE(ss.translation_period()==L);

    }

    SECTION("period 4 once more"){

        s.set_configuration({1, 0, 0, 1, 1, 0, 0, 1});

        auto ss = translated_state(s);

        REQUIRE(ss.translation_period()==4);

    }
}

TEST_CASE("contains", "[translated_state]")
{
    const int L = 8;
    const int d = 2;
    product_state s = product_state(L,d);

    SECTION("contains true"){

        s.set_configuration({1, 1, 0, 1, 1, 0, 1, 1});

        auto ss = translated_state(s);

        s.set_configuration({0, 1, 1, 0, 1, 1, 1, 1});

        REQUIRE(ss.contains(s));

        s.set_configuration({1, 0, 1, 1, 0, 1, 1, 1});

        REQUIRE(ss.contains(s));

        s.set_configuration({1, 1, 0, 1, 1, 0, 1, 1});

        REQUIRE(ss.contains(s));

        s.set_configuration({1, 1, 1, 0, 1, 1, 0, 1});

        REQUIRE(ss.contains(s));

        s.set_configuration({1, 0, 1, 1, 1, 1, 0, 1});

        REQUIRE(ss.contains(s));

    }

    SECTION("contains false"){

        s.set_configuration({1, 1, 0, 1, 1, 0, 1, 1});

        auto ss = translated_state(s);

        s.set_configuration({1, 1, 0, 1, 1, 1, 0, 1});

        REQUIRE_FALSE(ss.contains(s));

    }

    SECTION("contains error L"){

        s.set_configuration({1, 1, 0, 1, 1, 1, 1, 1});

        auto ss = translated_state(s);

        auto sp = product_state(L+1, d);

        REQUIRE_THROWS_AS(ss.contains(sp), mismatching_system_size);

        sp = product_state(L-1, d);

        REQUIRE_THROWS_AS(ss.contains(sp), mismatching_system_size);

    }

    SECTION("contains error d"){

        s.set_configuration({1, 1, 0, 1, 1, 1, 1, 1});

        auto ss = translated_state(s);

        auto sp = product_state(L, d+1);

        REQUIRE_THROWS_AS(ss.contains(sp), mismatching_local_dimensions);

    }

    SECTION("== and != true"){

        s.set_configuration({1, 1, 0, 1, 1, 0, 1, 1});

        auto ss = translated_state(s);

        auto s1 = product_state(L,d);
        s1.set_configuration({1, 0, 1, 1, 1, 1, 0, 1});

        auto ss1 = translated_state(s1);

        REQUIRE(ss == ss1);
        REQUIRE_FALSE( ss != ss1);

    }

    SECTION("== and != false"){

        s.set_configuration({1, 1, 0, 1, 1, 0, 1, 1});

        auto ss = translated_state(s);

        auto s1 = product_state(L,d);
        s1.set_configuration({1, 1, 1, 1, 1, 1, 0, 1});

        auto ss1 = translated_state(s1);

        REQUIRE_FALSE(ss == ss1);
        REQUIRE( ss != ss1);

    }

}

TEST_CASE("components","[translated_state]"){

    int L = 8;

    sp_spin_1_2_basis spin_basis = sp_spin_1_2_basis(L);

    const int d = 2;

    product_state s = product_state(L,d);

    SECTION("period 1"){

        s.set_configuration({1, 1, 1, 1, 1, 1, 1, 1});

        auto ss = translated_state(s);

        std::vector<component> components = ss.components(spin_basis);

        bool flag = true;

        //check number of components
        REQUIRE(components.size()==ss.translation_period());

        //check each component is indeed a component
        for (component comp: components){
            if (!ss.contains(spin_basis[comp.index])){
                flag = false;
                break;
            }
        }
        REQUIRE(flag);

        //check each component is distinct
        for (int i=0; i<components.size(); i++){
            for (int j=i+1; j<components.size(); j++){
                if (components[i].index == components[j].index){
                    flag = false;
                    break;
                }
            }
        }
        REQUIRE(flag);

    }

    SECTION("period 2"){

        s.set_configuration({1, 0, 1, 0, 1, 0, 1, 0});

        auto ss = translated_state(s);

        std::vector<component> components = ss.components(spin_basis);

        bool flag = true;

        //check number of components
        REQUIRE(components.size()==ss.translation_period());

        //check each component is indeed a component
        for (component comp: components){
            if (!ss.contains(spin_basis[comp.index])){
                flag = false;
                break;
            }
        }
        REQUIRE(flag);

        //check each component is distinct
        for (int i=0; i<components.size(); i++){
            for (int j=i+1; j<components.size(); j++){
                if (components[i].index == components[j].index){
                    flag = false;
                    break;
                }
            }
        }
        REQUIRE(flag);

    }

    SECTION("period 4"){

        s.set_configuration({1, 0, 0, 1, 1, 0, 0, 1});

        auto ss = translated_state(s);

        std::vector<component> components = ss.components(spin_basis);

        bool flag = true;

        //check number of components
        REQUIRE(components.size()==ss.translation_period());

        //check each component is indeed a component
        for (component comp: components){
            if (!ss.contains(spin_basis[comp.index])){
                flag = false;
                break;
            }
        }
        REQUIRE(flag);

        //check each component is distinct
        for (int i=0; i<components.size(); i++){
            for (int j=i+1; j<components.size(); j++){
                if (components[i].index == components[j].index){
                    flag = false;
                    break;
                }
            }
        }
        REQUIRE(flag);

    }

    SECTION("period L"){

        s.set_configuration({1, 0, 1, 1, 1, 0, 0, 1});

        auto ss = translated_state(s);

        std::vector<component> components = ss.components(spin_basis);

        bool flag = true;

        //check number of components
        REQUIRE(components.size()==ss.translation_period());

        //check each component is indeed a component
        for (component comp: components){
            if (!ss.contains(spin_basis[comp.index])){
                flag = false;
                break;
            }
        }
        REQUIRE(flag);

        //check each component is distinct
        for (int i=0; i<components.size(); i++){
            for (int j=i+1; j<components.size(); j++){
                if (components[i].index == components[j].index){
                    flag = false;
                    break;
                }
            }
        }
        REQUIRE(flag);

    }


}