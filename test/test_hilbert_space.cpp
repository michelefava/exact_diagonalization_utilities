#include "catch.hpp"

#include "../includes/product_state.hpp"
#include "../includes/basis.hpp"
#include "../includes/numbers.hpp"

#include <iostream>

using namespace std;

TEST_CASE("sparse and full spin-1/2 spaces are the same","[basis]"){
    int L = 5;
    int d = 2;
    dense_basis<product_state> ds(L, d, pow(2,L));
    build_full_basis(ds);

    sp_spin_1_2_basis ss = sp_spin_1_2_basis(L);

    REQUIRE(ss.dim()==ds.dim());
    REQUIRE(ds.dim()==pow(2,L));

    SECTION("check the states are the same"){

        bool flag = true;

        for (int i=0; i<ss.dim(); i++){

            if (ss[i] != ds[i])
                flag = false;
        }

        REQUIRE(flag);
    }

    SECTION("check get_index dense"){
        bool flag = true;

        for (int i=0; i<ds.dim(); i++){
            auto state = ds[i];
            if (ds.get_index(state)!=i)
                flag = false;
        }

        REQUIRE(flag);
    }

    SECTION("check get_index sparse"){
        bool flag = true;

        for (int i=0; i<ss.dim(); i++){
            auto state = ss[i];
            if (ss.get_index(state)!=i)
                flag = false;
        }

        REQUIRE(flag);
    }
}


TEST_CASE("sparse and full Boson spaces are the same","[basis]"){
    int L = 6;
    int N = L+2;
    int d = N+1;
    dense_basis<product_state> ds(L, d, binomial(2*L-1,L-1));
    build_full_U1_basis(ds,N);

    //std::cout << ds;

    sp_U1_basis ss = sp_U1_basis(L,N);

    REQUIRE(ss.dim()==ds.dim());
    REQUIRE(ds.dim()==binomial(N+L-1,L-1));

    SECTION("check the states are the same"){
        
        bool flag = true;
        const int D = ss.dim();
        for (int i=0; i<D; i++){
            cout<< ss[i];
            cout<< ds[i]<<"\n";
            if (ss[i] != ds[i])
                flag = false;
        }

        REQUIRE(flag);
    }

    SECTION("check get_index dense"){
        bool flag = true;

        for (int i=0; i<ds.dim(); i++){
            auto state = ds[i];
            if (ds.get_index(state)!=i)
                flag = false;
        }

        REQUIRE(flag);
    }

    SECTION("check get_index sparse"){
        bool flag = true;

        for (int i=0; i<ss.dim(); i++){
            auto state = ss[i];
            if (ss.get_index(state)!=i)
                flag = false;
        }

        REQUIRE(flag);
    }
}