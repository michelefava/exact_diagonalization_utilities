#include "catch.hpp"

#include "../includes/glob_operator.hpp"
#include "../includes/translated_state.hpp"
#include "../includes/basis_build.hpp"

TEST_CASE("projector identities","[projector]"){

    int L = 8;

    using namespace std;

    shared_ptr<sp_spin_1_2_basis> spin_basis = make_shared<sp_spin_1_2_basis>(sp_spin_1_2_basis(L));

    const int d = 2;

    shared_ptr<dense_basis<translated_state>> trans_basis = 
        make_shared<dense_basis<translated_state>>(dense_basis<translated_state>(L,d,pow(2,L)/L*2));

    symmetrize_basis(*spin_basis, *trans_basis);

    auto P = projector<translated_state>(spin_basis,trans_basis);

    REQUIRE(P.mat.n_rows == trans_basis->dim());

    REQUIRE(P.mat.n_cols == spin_basis->dim());

    //check that P is a projector
    //P * P^\dag =1
    auto Psq = P*P.dag();
    auto Psq_m = Psq.mat;

    int D = trans_basis->dim();
    REQUIRE(arma::norm(Psq_m - arma::eye<arma::sp_cx_mat>(D,D)) < 1e-10);

}