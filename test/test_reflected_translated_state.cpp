#include "catch.hpp"

#include "../includes/reflected_translated_state.hpp"
#include <cmath>

const Real tol = 1e-10;

TEST_CASE("reflection period, odd sites", "[reflected_translated_state]")
{
    const int L = 9;
    const int d = 2;
    product_state s = product_state(L,d);

    SECTION("1"){

        s.set_configuration({1, 1, 1, 1, 1, 1, 1, 1, 1});

        auto ss = reflected_translated_state(s);

        REQUIRE(ss.translation_period()==1);
        REQUIRE(ss.reflection_period()==1);

    }

    SECTION("2"){

        s.set_configuration({0, 1, 1, 1, 1, 1, 1, 1, 1});

        auto ss = reflected_translated_state(s);

        REQUIRE(ss.translation_period()==L);
        REQUIRE(ss.reflection_period()==1);

    }

    SECTION("3"){

        s.set_configuration({0, 1, 1, 0, 1, 0, 1, 1, 1});

        auto ss = reflected_translated_state(s);

        REQUIRE(ss.translation_period()==L);
        REQUIRE(ss.reflection_period()==2);

    }

    SECTION("4"){

        s.set_configuration({0, 1, 1, 0, 1, 1, 0, 1, 1});

        auto ss = reflected_translated_state(s);

        REQUIRE(ss.translation_period()==3);
        REQUIRE(ss.reflection_period()==1);

    }

    SECTION("5"){

        s.set_configuration({0, 0, 1, 0, 1, 1, 0, 0, 0});

        auto ss = reflected_translated_state(s);

        REQUIRE(ss.translation_period()==L);
        REQUIRE(ss.reflection_period()==2);

    }
}

TEST_CASE("reflection period, even sites", "[reflected_translated_state]")
{
    const int L = 8;
    const int d = 2;
    product_state s = product_state(L,d);

    SECTION("1"){

        s.set_configuration({1, 1, 1, 1, 1, 1, 1, 1});

        auto ss = reflected_translated_state(s);

        REQUIRE(ss.translation_period()==1);
        REQUIRE(ss.reflection_period()==1);

    }

    SECTION("2"){

        s.set_configuration({0, 1, 0, 1, 0, 1, 0, 1});

        auto ss = reflected_translated_state(s);

        REQUIRE(ss.translation_period()==2);
        REQUIRE(ss.reflection_period()==1);

    }

    SECTION("3"){

        s.set_configuration({0, 1, 1, 0, 1, 1, 0, 1});

        auto ss = reflected_translated_state(s);

        REQUIRE(ss.translation_period()==L);
        REQUIRE(ss.reflection_period()==1);

    }

    SECTION("4"){

        s.set_configuration({0, 1, 1, 1, 0, 1, 0, 0});

        auto ss = reflected_translated_state(s);

        REQUIRE(ss.translation_period()==L);
        REQUIRE(ss.reflection_period()==2);

    }

    SECTION("5"){

        s.set_configuration({0, 1, 1, 0, 0, 1, 1, 0});

        auto ss = reflected_translated_state(s);

        REQUIRE(ss.translation_period()==4);
        REQUIRE(ss.reflection_period()==1);

    }

    
}

TEST_CASE("reflection period, 10 sites", "[reflected_translated_state]")
{
    const int L = 10;
    const int d = 2;
    product_state s = product_state(L,d);

    SECTION("1"){

        s.set_configuration({1, 1, 0, 1, 0, 1, 1, 0 ,1 , 0});

        auto ss = reflected_translated_state(s);

        REQUIRE(ss.translation_period()==5);
        REQUIRE(ss.reflection_period()==1);

    }
}

TEST_CASE("reflection period, 10 sites d=3", "[reflected_translated_state]")
{
    const int L = 10;
    const int d = 3;
    product_state s = product_state(L,d);

    SECTION("1"){

        s.set_configuration({1, 2, 0, 0, 0, 1, 2, 0 ,0 , 0});

        auto ss = reflected_translated_state(s);

        REQUIRE(ss.translation_period()==5);
        REQUIRE(ss.reflection_period()==2);

    }
}


TEST_CASE("contains, reflection period 1", "[reflected_translated_state]")
{
    const int L = 8;
    const int d = 2;
    product_state s = product_state(L,d);

    SECTION("contains true"){

        s.set_configuration({1, 1, 0, 1, 1, 0, 1, 1});

        auto ss = reflected_translated_state(s);

        s.set_configuration({0, 1, 1, 0, 1, 1, 1, 1});

        REQUIRE(ss.contains(s));

        s.set_configuration({1, 0, 1, 1, 0, 1, 1, 1});

        REQUIRE(ss.contains(s));

        s.set_configuration({1, 1, 0, 1, 1, 0, 1, 1});

        REQUIRE(ss.contains(s));

        s.set_configuration({1, 1, 1, 0, 1, 1, 0, 1});

        REQUIRE(ss.contains(s));

        s.set_configuration({1, 0, 1, 1, 1, 1, 0, 1});

        REQUIRE(ss.contains(s));

    }

    SECTION("contains false"){

        s.set_configuration({1, 1, 0, 1, 1, 0, 1, 1});

        auto ss = reflected_translated_state(s);

        s.set_configuration({1, 1, 0, 1, 1, 1, 0, 1});

        REQUIRE_FALSE(ss.contains(s));

    }

    SECTION("contains error L"){

        s.set_configuration({1, 1, 0, 1, 1, 1, 1, 1});

        auto ss = reflected_translated_state(s);

        auto sp = product_state(L+1, d);

        REQUIRE_THROWS_AS(ss.contains(sp), mismatching_system_size);

        sp = product_state(L-1, d);

        REQUIRE_THROWS_AS(ss.contains(sp), mismatching_system_size);

    }

    SECTION("contains error d"){

        s.set_configuration({1, 1, 0, 1, 1, 1, 1, 1});

        auto ss = reflected_translated_state(s);

        auto sp = product_state(L, d+1);

        REQUIRE_THROWS_AS(ss.contains(sp), mismatching_local_dimensions);

    }

    SECTION("== and != true"){

        s.set_configuration({1, 1, 0, 1, 1, 0, 1, 1});

        auto ss = reflected_translated_state(s);

        auto s1 = product_state(L,d);
        s1.set_configuration({1, 0, 1, 1, 1, 1, 0, 1});

        auto ss1 = reflected_translated_state(s1);

        REQUIRE(ss == ss1);
        REQUIRE_FALSE( ss != ss1);

    }

    SECTION("== and != false"){

        s.set_configuration({1, 1, 0, 1, 1, 0, 1, 1});

        auto ss = reflected_translated_state(s);

        auto s1 = product_state(L,d);
        s1.set_configuration({1, 1, 1, 1, 1, 1, 0, 1});

        auto ss1 = reflected_translated_state(s1);

        REQUIRE_FALSE(ss == ss1);
        REQUIRE( ss != ss1);

    }

}

TEST_CASE("contains, reflection period 2", "[reflected_translated_state]"){
    
    int L=8;
    int d=3;
    product_state s = product_state(L,d);

    SECTION("contains true (1)"){

        s.set_configuration({2, 1, 0, 0, 2, 1, 0, 0});

        auto ss = reflected_translated_state(s);

        s.set_configuration({1, 2, 0, 0, 1, 2, 0, 0});

        REQUIRE(ss.contains(s));

    }

    SECTION("contains true (2)"){

        s.set_configuration({2, 1, 0, 0, 2, 1, 1, 0});

        auto ss = reflected_translated_state(s);

        s.set_configuration({1, 2, 0, 1, 1, 2, 0, 0});

        REQUIRE(ss.contains(s));

    }

    SECTION("contains false"){

        s.set_configuration({2, 1, 0, 0, 2, 1, 0, 0});

        auto ss = reflected_translated_state(s);

        s.set_configuration({1, 2, 1, 0, 1, 2, 0, 0});

        REQUIRE_FALSE(ss.contains(s));

    }

}

TEST_CASE("reflected_translated components","[reflected_translated_state]"){

    int L = 8;

    sp_spin_1_2_basis spin_basis = sp_spin_1_2_basis(L);

    const int d = 2;

    product_state s = product_state(L,d);

    SECTION("period 1, even"){

        s.set_configuration({1, 1, 1, 1, 1, 1, 1, 1});

        auto ss = reflected_translated_state(s);

        std::vector<component> components = ss.components(spin_basis);

        bool flag = true;

        //check number of components
        REQUIRE(components.size()==ss.translation_period());

        //check each component is indeed a component
        for (component comp: components){
            if (!ss.contains(spin_basis[comp.index])){
                flag = false;
                break;
            }
        }
        REQUIRE(flag);

        //check each component is distinct
        for (int i=0; i<components.size(); i++){
            for (int j=i+1; j<components.size(); j++){
                if (components[i].index == components[j].index){
                    flag = false;
                    break;
                }
            }
        }
        REQUIRE(flag);

    }

    SECTION("period 2, even"){

        s.set_configuration({1, 0, 1, 0, 1, 0, 1, 0});

        auto ss = reflected_translated_state(s);

        std::vector<component> components = ss.components(spin_basis);

        bool flag = true;

        //check number of components
        REQUIRE(components.size()==ss.translation_period());

        //check each component is indeed a component
        for (component comp: components){
            if (!ss.contains(spin_basis[comp.index])){
                flag = false;
                break;
            }
        }
        REQUIRE(flag);

        //check each component is distinct
        for (int i=0; i<components.size(); i++){
            for (int j=i+1; j<components.size(); j++){
                if (components[i].index == components[j].index){
                    flag = false;
                    break;
                }
            }
        }
        REQUIRE(flag);

    }

    SECTION("period 4, even"){

        s.set_configuration({1, 0, 0, 1, 1, 0, 0, 1});

        auto ss = reflected_translated_state(s);

        std::vector<component> components = ss.components(spin_basis);

        bool flag = true;

        //check number of components
        REQUIRE(components.size()==ss.translation_period());

        //check each component is indeed a component
        for (component comp: components){
            if (!ss.contains(spin_basis[comp.index])){
                flag = false;
                break;
            }
        }
        REQUIRE(flag);

        //check each component is distinct
        for (int i=0; i<components.size(); i++){
            for (int j=i+1; j<components.size(); j++){
                if (components[i].index == components[j].index){
                    flag = false;
                    break;
                }
            }
        }
        REQUIRE(flag);

        //check overlap of each component
        for (int i=0; i<components.size(); i++){
            if (abs(components[i].overlap - 1/sqrt(4))>tol){
                flag = false;
                break;
            }
        }
        REQUIRE(flag);

    }

    SECTION("period 4, even"){

        s.set_configuration({1, 0, 1, 1, 1, 0, 1, 1});

        auto ss = reflected_translated_state(s);

        std::vector<component> components = ss.components(spin_basis);

        bool flag = true;

        //check number of components
        REQUIRE(components.size()==ss.translation_period());

        //check each component is indeed a component
        for (component comp: components){
            if (!ss.contains(spin_basis[comp.index])){
                flag = false;
                break;
            }
        }
        REQUIRE(flag);

        //check each component is distinct
        for (int i=0; i<components.size(); i++){
            for (int j=i+1; j<components.size(); j++){
                if (components[i].index == components[j].index){
                    flag = false;
                    break;
                }
            }
        }
        REQUIRE(flag);

        //check overlap of each component
        for (int i=0; i<components.size(); i++){
            if (abs(components[i].overlap - 1/sqrt(4))>tol){
                flag = false;
                break;
            }
        }
        REQUIRE(flag);

    }

    SECTION("period L, not even"){

        s.set_configuration({1, 1, 1, 0, 1, 0, 0, 0});

        auto ss = reflected_translated_state(s);

        std::vector<component> components = ss.components(spin_basis);

        bool flag = true;

        //check number of components
        REQUIRE(components.size()==ss.translation_period()*ss.reflection_period());

        //check each component is indeed a component
        for (component comp: components){
            if (!ss.contains(spin_basis[comp.index])){
                flag = false;
                break;
            }
        }
        REQUIRE(flag);

        //check each component is distinct
        for (int i=0; i<components.size(); i++){
            for (int j=i+1; j<components.size(); j++){
                if (components[i].index == components[j].index){
                    flag = false;
                    break;
                }
            }
        }
        REQUIRE(flag);

        //check overlap of each component
        for (int i=0; i<components.size(); i++){
            if (abs(components[i].overlap - 1/sqrt(2*L))>tol){
                flag = false;
                break;
            }
        }
        REQUIRE(flag);

    }


}