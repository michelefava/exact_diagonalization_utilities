#include "catch.hpp"

#include "../includes/product_state.hpp"
#include "../includes/basis.hpp"
#include "../includes/boson_operators.hpp"
#include "../includes/pauli_operators.hpp"

#include <armadillo>
#include <iostream>

TEST_CASE("Bosonic identities, site 3,5","[bosonic operators]"){
    const int L = 7;
    const int N = 10;
    const int site1 = 3;
    const int site2 = 5;
    const double tolerance = 1e-10;

    auto base = std::make_shared<sp_U1_basis>(sp_U1_basis(L,N));
    auto base_Nm1 = std::make_shared<sp_U1_basis>(sp_U1_basis(L,N-1));

    auto a1 = boson::a(site1, base, base_Nm1);
    auto adag1 = boson::adag(site1, base_Nm1, base);
    auto adag2 = boson::adag(site2, base_Nm1, base);
    auto n1 = boson::n(site1, base);
    auto hop12 = boson::hop(site1,site2,base);

    auto id = identity(base);

    REQUIRE(a1.in_basis() == base);
    REQUIRE(adag1.in_basis() == base_Nm1);
    REQUIRE(a1.out_basis() == base_Nm1);
    REQUIRE(adag1.out_basis() == base);

    SECTION("adag1 * a1 = n1"){
        REQUIRE(arma::norm((adag1*a1-n1).mat)< tolerance);
    }

    SECTION("adag2 * a1 = hop12"){
        REQUIRE(arma::norm((adag2*a1-hop12).mat)< tolerance);
    }



}

TEST_CASE("Bosonic identities, site 0,6","[bosonic operators]"){
    const int L = 7;
    const int N = 10;
    const int site1 = 0;
    const int site2 = 6;
    const double tolerance = 1e-10;

    auto base = std::make_shared<sp_U1_basis>(sp_U1_basis(L,N));
    auto base_Nm1 = std::make_shared<sp_U1_basis>(sp_U1_basis(L,N-1));

    auto a1 = boson::a(site1, base, base_Nm1);
    auto adag1 = boson::adag(site1, base_Nm1, base);
    auto adag2 = boson::adag(site2, base_Nm1, base);
    auto n1 = boson::n(site1, base);
    auto hop12 = boson::hop(site1,site2,base);

    auto id = identity(base);

    REQUIRE(a1.in_basis() == base);
    REQUIRE(adag1.in_basis() == base_Nm1);
    REQUIRE(a1.out_basis() == base_Nm1);
    REQUIRE(adag1.out_basis() == base);

    SECTION("adag1 * a1 = n1"){
        REQUIRE(arma::norm((adag1*a1-n1).mat)< tolerance);
    }

    SECTION("adag2 * a1 = hop12"){
        REQUIRE(arma::norm((adag2*a1-hop12).mat)< tolerance);
    }



}