#ifndef __BASIS_H__
#define __BASIS_H__

#include <vector>
#include <math.h>
#include "product_state.hpp"
#include "numbers.hpp"



// virtual class specifying the interface of a generic Hilbert space basis
// the template state is meant to be either a product state or a symmetrized state
template <class state>
class basis{
    public:

        virtual int dim() const = 0;

        virtual int get_index(const state& s) const = 0;

        virtual state operator[](int j) const = 0;

        virtual int L() const = 0;

        virtual int d() const = 0;
};

template <class state>
std::ostream& operator<<(std::ostream& os, const basis<state>& b){
    for (int i=0; i<b.dim(); i++){
        os << b[i] << "\n";
    }
    return os;
}


// generic class detailing the behaviour of a "dense" Hilbert space basis,
// where a full basis is stored in memory
template <class state>
class dense_basis: public basis<state>{
    public:

        dense_basis (const int L, const int d, const int dim_extimate): L_(L), d_(d){
            basis_ = std::vector<state>(0);
            basis_.reserve(dim_extimate);
        }

        void add_to_basis (const state& new_state){
            if (new_state.d() != d_)
                throw mismatching_local_dimension();
            
            if (new_state.L() != L_)
                throw mismatching_system_size();

            basis_.push_back(new_state);
        }

        int dim() const{
            return basis_.size();
        }

        int get_index(const state& s) const{
            for (int i=0;  i<dim(); i++){
                if (s == basis_.at(i))
                    return i;
            }

            throw state_not_in_basis();
        }

        bool contain_state(const state& s) const{
            for (int i=0;  i<dim(); i++){
                if (s == basis_.at(i))
                    return true;
            }

            return false;
        }

        state operator[](int j) const{
            if (j>=dim() || j<0)
                throw state_out_of_range();
            
            return basis_.at(j);
        }

        int L() const{return L_;}
        int d() const{return d_;}

        class mismatching_local_dimension{};

        class mismatching_system_size{};

        class state_not_in_basis{};

        class state_out_of_range{};
    
    private:
        int L_ = 0;
        int d_ = 2;
        std::vector<state> basis_;
};

//create a dense Hilbert space, consisting of all possible product states
void build_full_basis(dense_basis<product_state>& space);

//create a dense Hilbert space, consisting of all possible product states with sum N fixed
void build_full_U1_basis(dense_basis<product_state>& space, const int N);


// sparse spin-1/2 Hilbert space (without conservation laws)
// the basis is not stred in memory, but elements are constructed on the fly
class sp_spin_1_2_basis: public basis<product_state>{
    public:

        sp_spin_1_2_basis (const int L): L_(L) {}

        int dim() const{
            return pow(2,L());
        }

        int L() const{return L_;}

        int get_index(const product_state& s) const{
            if (s.d()!=d_)
                throw mismatching_local_dimensions();

            if (s.L()!=L())
                throw mismatching_system_size();

            int ind = 0;
            for (int i=0; i<L(); i++)
                ind += s.get_site(i) * pow(2,i);

            return ind;
            
        }

        product_state operator[](int j) const{
            if (j>=dim() || j<0)
                throw state_out_of_range();

            product_state s = product_state(L(),d_);

            for (int i=0; i<L(); i++){
                s.set_site(i, j%2);
                j /= 2;
            }

            return s;
            
        }

        int d() const {return 2;}

        class mismatching_local_dimension{};

        class mismatching_system_size{};

        class state_not_in_basis{};

        class state_out_of_range{};
    
    private:
        int L_ = 0;
        static const int d_ = 2;
};

// sparse spin-1/2 Hilbert space (without conservation laws)
// the basis is not stred in memory, but elements are constructed on the fly
class sp_U1_basis: public basis<product_state>{
    public:

        sp_U1_basis (const int L, const int N): L_(L), N_(N), d_(N+1){}

        int dim() const{
            return binomial(N_+L_-1, L_-1);
        }

        int L() const{return L_;}

        int get_index(const product_state& s) const{
            if (s.d()!=d_)
                throw mismatching_local_dimensions();

            if (s.L()!=L())
                throw mismatching_system_size();

            int ind = 0;

            int N_right = N_;
            for (int i=0; i<L(); i++){
                int right_sites = L_-i-1;
                for (int j=0; j<s.get_site(i); j++){
                    int remaining_N = N_right - j;
                    ind += dim_of_space(right_sites, remaining_N);
                }
                N_right -= s.get_site(i);
            }
                    

            return ind;
            
        }

        product_state operator[](int ind) const{
            
            if (ind>=dim() || ind<0)
                throw state_out_of_range();

            product_state s = product_state(L(),d_);

            int N_right = N_;
            for (int i=0; i<L(); i++){
                int right_sites = L_-i-1;
                int j = 0;

                for (j=0; j<d_ && j<=N_right; j++){

                    int remaining_N = N_right - j;

                    int states = dim_of_space(right_sites , remaining_N );

                    if (states>ind)
                        break;
                    else
                        ind -= states;
                }
                //if gone out of the loop without passing by "break" then j has been increased too much
                if (j==d_ || j==N_right+1) j--;

                s.set_site(i, j);
                N_right -= j;
            }

            return s;
            
        }

        int d() const {return d_;}

        class mismatching_local_dimension{};

        class mismatching_system_size{};

        class state_not_in_basis{};

        class state_out_of_range{};
    
    private:
        int L_ = 0;
        int N_ = 0;
        int d_ = N_+1;

        //returns the simension oof a space with L sites and N sites
        int dim_of_space (const int L, const int N) const{
            if (L<0)
                throw std::runtime_error("call to dim_of_space with L<0");

            if (L==0 && N==0)
                return 1;
            
            if ((L==0) ^ (N==0))
                return 0;

            return binomial(L+N-1,L-1);

        }
};


#endif