#ifndef __SYMMETRIES_IMPLEMENTATION_H__
#define __SYMMETRIES_IMPLEMENTATION_H__

#include "product_state.hpp"

void reflect_state(product_state& state);

void shift_state_to_right(product_state &state);

class reflection_symmetry{
    public:
        void apply_symmetry(product_state& state) const{
            reflect_state(state);
        }
};

class translation_symmetry{
    public:
        void apply_symmetry(product_state& state) const{
            shift_state_to_right(state);
        }
};

#endif