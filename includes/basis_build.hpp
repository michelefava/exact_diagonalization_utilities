#ifndef __BASIS_BUILD_H__
#define __BASIS_BUILD_H__

#include "symmetrized_state.hpp"
#include "basis.hpp"

//symmetrize the states in prod_basis and store them in sym_basis
//sym_basis needs to be empty when the function is called

template <class sym_state>
void symmetrize_basis(const basis<product_state>& prod_basis, dense_basis<sym_state>& sym_basis){

    if (sym_basis.dim()!=0)
        throw std::runtime_error("symmetrize_basis requires an empty sym_basis");

    std::vector<bool> already_taken(prod_basis.dim(), false);

    for (int i=0; i<prod_basis.dim(); i++){

        std::cout << "\b\b\b"<< 100 * i / prod_basis.dim()<<"%";

        if (!already_taken[i]){

            product_state sp = prod_basis[i];

            sym_state ss = sym_state(sp);

            sym_basis.add_to_basis(ss);

            std::vector<component> components = ss.components(prod_basis);

            for (auto c: components){
                already_taken[c.index] = true; 
            }



        }

    }
}



#endif