#ifndef __NUMBERS_H__
#define __NUMBERS_H__

#include <complex> 

typedef double Real;

typedef std::complex<Real> Complex;

inline int binomial(int n, int k) {

    if (n<0 || k<0 || k>n){
        throw std::runtime_error("Invalid arguments for binomial");
    }

    //Kudos to Sounak for suggesting this implementation!

    long int res=1;
    for (int i=1; i<=k; i++){
        res = res * n / i;
        n--;
    }

    return res;

}

#endif