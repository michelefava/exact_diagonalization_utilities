#ifndef __PRODUCT_STATE_H__
#define __PRODUCT_STATE_H__

#include <vector>
#include <iostream>

class mismatching_local_dimensions{};

class mismatching_system_size{};

class product_state{
    public:

        product_state(){};

        product_state( const int L, const int d): d_ (d) {
            rep_ = std::vector<int>(L);
        }



        void set_site(const int site, const int state){
            
            if (site >= L() || site < 0)
                throw invalid_site();
            
            if (state >= d_ || state < 0)
                throw invalid_state();

            rep_[site] = state;
            
        }

        int get_site(const int site) const{
            if (site >= L() || site < 0)
                throw invalid_site();

            return rep_[site];
        }

        int set_configuration(const std::vector<int> s){
            if (s.size()!=L())
                throw mismatching_system_size();

            for (int i=0; i<L(); i++)
                set_site(i,s[i]);
        }

        class invalid_site{};

        class invalid_state{};

        int d() const{return d_;}

        int L() const{return rep_.size();}

    private:

        //representation of the state in the computational basis
        std::vector<int> rep_ = std::vector<int>(0);

        //local Hilbert space dimension
        int d_ = 2;

};


bool operator==(const product_state& state1, const product_state& state2);

bool operator!=(const product_state &state1, const product_state &state2);

std::ostream& operator<< (std::ostream& os, const product_state& state);


#endif