#pragma once

#ifndef __TRANSLATED_STATE_H__
#define __TRANSLATED_STATE_H__

#include "product_state.hpp"
#include "symmetrized_state.hpp"
#include "symmetries_implementation.hpp"



//class representing a state symmetrized under translation only
class translated_state : public symmetrized_state<product_state, translation_symmetry>{

    public:

        translated_state(){}

        translated_state(const product_state& state_rep): symmetrized_state<product_state, translation_symmetry>(state_rep){}

        int translation_period(){
            return symmetry_period();
        }
        
};

#endif