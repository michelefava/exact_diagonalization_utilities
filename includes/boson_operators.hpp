#ifndef __BOSON_OPERATORS_H__
#define __BOSON_OPERATORS_H__

#include<armadillo>
#include "basis.hpp"
#include "product_state.hpp"
#include "glob_operator.hpp"
#include "numbers.hpp"

/*
    Construct bosonic creation, annihilation, number, hopping operators
    interpreting the Hilbert space as a (possibly truncated) bosonic system,
    where the n-th state of the computational basis represents a site occupied by n bosons
*/

///     WARNING: works only for non-trucated spaces at the moment

namespace boson{

    inline void check_arguments(const int site, const std::shared_ptr<basis<product_state>>& base){

        if (site<0 || site >= base->L())
            throw std::runtime_error("Attemp to construct bosonic operator acting on inexistent site.");
    }

    

    inline glob_operator<arma::sp_mat,product_state,product_state> 
    n(const int site, std::shared_ptr<basis<product_state>> base){

        check_arguments(site, base);

        const int D = base->dim();

        auto n = glob_operator<arma::sp_mat,product_state,product_state>(base, base);

        for (int i=0; i<D; i++){
            auto state = (*base)[i];
            n.mat(i,i) = state.get_site(site);
        }

        return n;

    }

    inline glob_operator<arma::sp_mat,product_state,product_state> 
    hop(const int from_site, const int to_site, std::shared_ptr<basis<product_state>> base){

        check_arguments(from_site, base);
        check_arguments(to_site, base);

        const int D = base->dim();

        auto hop = glob_operator<arma::sp_mat,product_state,product_state>(base, base);

        for (int i=0; i<D; i++){
            auto state = (*base)[i];
            int n_from = state.get_site(from_site);
            int n_to = state.get_site(to_site);

            if (n_from!=0){
                Real mat_el = sqrt(n_from * (n_to+1));

                state.set_site(from_site, n_from-1);
                state.set_site(to_site, n_to+1);

                int j = base->get_index(state);

                hop.mat(j,i) = mat_el;
            }
        }

        return hop;
    }

    inline glob_operator<arma::sp_mat,product_state,product_state> 
    a(const int site, std::shared_ptr<basis<product_state>> base_N, std::shared_ptr<basis<product_state>> base_Nm1){

        if ( base_N->L()!=base_Nm1->L() )
            throw mismatching_system_size();

        check_arguments(site, base_N);

        const int D = base_N->dim();

        auto a = glob_operator<arma::sp_mat,product_state,product_state>(base_N, base_Nm1);

        for (int i=0; i<D; i++){
            auto state = (*base_N)[i];
            int n = state.get_site(site);

            if (n!=0){
                Real mat_el = sqrt(n);          
                
                auto statep = product_state(base_N->L(),base_Nm1->d());

                for (int s = 0; s < base_N->L(); s++){
                    if (s!=site)
                        statep.set_site(s, state.get_site(s));
                    else
                        statep.set_site(s, n-1);
                    
                }

                int j = base_Nm1->get_index(statep);

                a.mat(j,i) = mat_el;
            }
        }

        return a;
    }

    inline glob_operator<arma::sp_mat,product_state,product_state> 
    adag(const int site, std::shared_ptr<basis<product_state>> base_N, std::shared_ptr<basis<product_state>> base_Np1){
        auto a_op = a(site,base_Np1,base_N);
        return a_op.dag();
    }

};

#endif