#ifndef __PAULI_OPERATORS_H__
#define __PAULI_OPERATORS_H__

#include<armadillo>
#include "basis.hpp"
#include "product_state.hpp"
#include "numbers.hpp"
#include "glob_operator.hpp"

/*
    Construct Pauli operators interpreting a d=2 Hilbert space as a spin-1/2 system.
    The computational basis is interpreted as 0=↑ and 1=↓. 
*/

inline void pauli_check_arguments(const int site, const std::shared_ptr<basis<product_state>> base){
    if (base->d()!=2)
        throw std::runtime_error("Pauli matrices defined onlyfor local Hilber space dimension 2.");

    if (site<0 || site >= base->L())
        throw std::runtime_error("Attemp to construct Pauli operator acting on inexistent site.");
}

inline glob_operator<arma::sp_mat,product_state,product_state> 
identity(std::shared_ptr<basis<product_state>> base){
    
    const int D = base->dim();

    auto id = glob_operator<arma::sp_mat,product_state,product_state>(base, base);

    for (int i=0; i<D; i++){
        id.mat(i,i) = 1;
    }

    return id;

}

inline glob_operator<arma::sp_mat,product_state,product_state> 
sigmaz(const int site, std::shared_ptr<basis<product_state>> base){

    pauli_check_arguments(site, base);

    const int D = base->dim();

    auto sz = glob_operator<arma::sp_mat,product_state,product_state>(base, base);

    for (int i=0; i<D; i++){
        auto state = (*base)[i];
        sz.mat(i,i) = (state.get_site(site)==0? 1 : -1);
    }

    return sz;

}

inline glob_operator<arma::sp_mat,product_state,product_state> 
sigmax(const int site, std::shared_ptr<basis<product_state>> base){

    pauli_check_arguments(site, base);

    const int D = base->dim();

    auto sx = glob_operator<arma::sp_mat,product_state,product_state>(base, base);

    for (int i=0; i<D; i++){
        auto state = (*base)[i];
        int s = state.get_site(site);
        s = (s==0? 1 : 0);
        state.set_site(site, s);

        int j = base->get_index(state);

        sx.mat(i,j) = 1;
    }

    return sx;
}

inline glob_operator<arma::sp_cx_mat,product_state,product_state> 
sigmay(const int site, std::shared_ptr<basis<product_state>> base){

    pauli_check_arguments(site, base);

    const int D = base->dim();

    auto sy = glob_operator<arma::sp_cx_mat,product_state,product_state>(base, base);

    for (int i=0; i<D; i++){
        auto state = (*base)[i];
        int s = state.get_site(site);
        s = (s==0? 1 : 0);
        state.set_site(site, s);

        int j = base->get_index(state);
        
        Complex im {0, 1};
        sy.mat(i,j) = (s==0? im: -im);
    }

    return sy;
}

inline glob_operator<arma::sp_mat,product_state,product_state> 
sigma_plus(const int site, std::shared_ptr<basis<product_state>> base){

    pauli_check_arguments(site, base);

    const int D = base->dim();

    auto sp = glob_operator<arma::sp_mat,product_state,product_state>(base, base);

    for (int i=0; i<D; i++){
        auto state = (*base)[i];
        int s = state.get_site(site);
        if (s == 0){
            state.set_site(site, 1);

            int j = base->get_index(state);

            sp.mat(i,j) = 1;
        }
    }

    return sp;
}

inline glob_operator<arma::sp_mat,product_state,product_state> 
sigma_minus(const int site, std::shared_ptr<basis<product_state>> base){

    pauli_check_arguments(site, base);

    const int D = base->dim();

    auto sm = glob_operator<arma::sp_mat,product_state,product_state>(base, base);

    for (int i=0; i<D; i++){
        auto state = (*base)[i];
        int s = state.get_site(site);
        if (s == 1){
            state.set_site(site, 0);

            int j = base->get_index(state);

            sm.mat(i,j) = 1;
        }
    }

    return sm;
}

#endif