#ifndef __SYMMETRIZED_STATE_H__
#define __SYMMETRIZED_STATE_H__

#include "product_state.hpp"
#include "basis.hpp"
#include "numbers.hpp"

struct component{
    int index;
    Complex overlap;
};

//class for a generic symmetrized state
//to be used the apply_symmetry function must be implemented
//sym_state2 is another symmetrized state
//sym_impl is a class implementing the apply_symmetry() function
template<class sym_state2, class symm_impl>
class symmetrized_state{
    public:

        symmetrized_state(){}

        symmetrized_state(const product_state& state_rep): state_(state_rep){
            symmetry_period_ = compute_symmetry_period();
        }

        //applying the symmetry operation, after how many steps does it go back to the original one?
        int symmetry_period() const{
            return symmetry_period_;
        }

        //check if the state prod_s can be obtained by applying the symmetry on state_rep_
        bool contains(const product_state& prod_s) const{
            auto s = state_;
            for (int i=0; i<symmetry_period_; i++){
                if (s.contains(prod_s))
                    return true;
                
                apply_symmetry(s);
            }
            return false;
        }

        int L()const{
            return state_.L();
        }

        int d() const{
            return state_.d();
        }

        product_state representative() const{
            return state_.representative();
        }

        std::vector<component> components(const basis<product_state>& prod_basis) const{
            
            auto s = state_;

            std::vector<component> overlap = s.components(prod_basis);
            apply_symmetry(s);

            for (int i=1; i<symmetry_period_; i++){
                auto ov_s = s.components(prod_basis);
                std::move(ov_s.begin(), ov_s.end(), std::back_inserter(overlap));
                apply_symmetry(s);
            }

            for (int i=0; i<overlap.size(); i++){
                overlap.at(i).overlap /= sqrt(symmetry_period_);
            }

            return overlap;
        }

        friend
        bool operator==(const symmetrized_state<sym_state2, symm_impl>& state1, const symmetrized_state<sym_state2, symm_impl>& state2){
            return state1.contains(state2.representative());
        }

        friend
        bool operator!=(const symmetrized_state<sym_state2, symm_impl>& state1, const symmetrized_state<sym_state2, symm_impl>& state2){
            return !(state1==state2);
        }

    protected:

        sym_state2 state_ = sym_state2();

    private:

        int compute_symmetry_period() const{
            auto state_copy = state_;
            int period = 1;
            apply_symmetry(state_copy);
            while (state_copy!=state_){
                apply_symmetry(state_copy);
                period++;
            }
            return period;
        }

        symm_impl symmetry_implementation = symm_impl();

        void apply_symmetry(product_state& prod_s) const{
            symmetry_implementation.apply_symmetry(prod_s);
        };

        void apply_symmetry(sym_state2& s) const{
            auto s_rep = s.representative();
            apply_symmetry(s_rep);
            s = sym_state2(s_rep);
        }

        int symmetry_period_ = 0;

};

//specialization of the symmetrized_state template
template<class symm_impl>
class symmetrized_state<product_state, symm_impl>{
    public:

        symmetrized_state(){}

        symmetrized_state(const product_state& state_rep): state_(state_rep){
            symmetry_period_ = compute_symmetry_period();
        }

        //applying the symmetry operation, after how many steps does it go back to the original one?
        int symmetry_period() const{
            return symmetry_period_;
        }

        //check if the state prod_s can be obtained by applying the symmetry on state_rep_
        bool contains(const product_state& prod_s) const{
            auto s = state_;
            for (int i=0; i<symmetry_period_; i++){
                if (s == prod_s)
                    return true;
                
                apply_symmetry(s);
            }
            return false;
        }

        int L() const{
            return state_.L();
        }

        int d() const{
            return state_.d();
        }

        product_state representative() const{
            return state_;
        }

        std::vector<component> components(const basis<product_state>& prod_basis) const{

            auto state_copy = state_;
            auto components = std::vector<component>(0);
            for (int i = 0; i<symmetry_period_; i++){
                int curr_ind =  prod_basis.get_index(state_copy);
                Complex curr_overlap = {1/std::sqrt(symmetry_period_), 0};
                component curr_component = {curr_ind, curr_overlap};
                components.push_back(curr_component);
                apply_symmetry(state_copy);
            }

            return components;
        }

        friend
        bool operator==(const symmetrized_state<product_state, symm_impl>& state1, const symmetrized_state<product_state, symm_impl>& state2){
            return state1.contains(state2.representative());
        }

        friend
        bool operator!=(const symmetrized_state<product_state, symm_impl>& state1, const symmetrized_state<product_state, symm_impl>& state2){
            return !(state1==state2);
        }

    protected:

        product_state state_ = product_state();

    private:

        int compute_symmetry_period() const{
            auto state_copy = state_;
            int period = 1;
            apply_symmetry(state_copy);
            while (state_copy!=state_){
                apply_symmetry(state_copy);
                period++;
            }
            return period;
        }

        symm_impl symmetry_implementation = symm_impl();

        void apply_symmetry(product_state& prod_s) const{
            symmetry_implementation.apply_symmetry(prod_s);
        };

        int symmetry_period_ = 0;

};


#endif