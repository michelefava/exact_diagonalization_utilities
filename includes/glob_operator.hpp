#ifndef __GLOB_OPERATOR_H__
#define __GLOB_OPERATOR_H__

#include <memory>
#include <armadillo>

#include "basis.hpp"
#include "symmetrized_state.hpp"



class incompatible_product{};

//a glob_operator is defined by
//      a basis in the domain
//      a basis in the image
//      a matrix of type mat_t specifying the matrix elements (relying on Armadillo)
template<class mat_t, class state_in, class state_out>
class glob_operator{

    public:

        glob_operator(std::shared_ptr<basis<state_in>> in_basis, std::shared_ptr<basis<state_out>> out_basis){
            out_basis_ = out_basis;
            in_basis_ = in_basis;
            mat = arma::zeros<mat_t>(out_basis->dim(),in_basis->dim());
        }


        std::shared_ptr<basis<state_in>> in_basis() const {return in_basis_;}

        std::shared_ptr<basis<state_out>> out_basis() const {return out_basis_;}

        mat_t mat;

        glob_operator<mat_t, state_out, state_in> dag(){
            auto dag_op = glob_operator<mat_t, state_out, state_in>(out_basis_, in_basis_);
            dag_op.mat = mat.t();

            return dag_op;
        }

        class incompatible_sum{};

        inline glob_operator<mat_t, state_in, state_out>&
        operator*=(Real rhs) {
            this->mat *= rhs;
            return *this;
        }

        inline glob_operator<mat_t, state_in, state_out>&
        operator*=(Complex rhs) {
            static_assert( 
                (std::is_same<mat_t,arma::cx_mat>::value || std::is_same<mat_t,arma::sp_cx_mat>::value),
                "Multiplication by complex require an explicit cast to a complex operator");
                
            this->mat *= rhs;
            return *this;
        }

        inline glob_operator<mat_t, state_in, state_out>&
        operator+=(const glob_operator<mat_t, state_in, state_out>& rhs) {

            if ( (rhs.in_basis()!=this->in_basis()) || (rhs.out_basis()!=this->out_basis()) )
                throw incompatible_sum();
    
            this->mat += rhs.mat;

            return *this;

        }

        inline glob_operator<mat_t, state_in, state_out>&
        operator-=(const glob_operator<mat_t, state_in, state_out>& rhs) {

            if ( (rhs.in_basis()!=this->in_basis()) || (rhs.out_basis()!=this->out_basis()) )
                throw incompatible_sum();
    
            this->mat -= rhs.mat;

            return *this;

        }
 
    private:
        std::shared_ptr<basis<state_in>> in_basis_;
        std::shared_ptr<basis<state_out>> out_basis_;
};

template<class state_in, class state_out>
glob_operator<arma::sp_cx_mat, state_in, state_out>
to_complex(const glob_operator<arma::sp_mat, state_in, state_out>& op){

    auto c_op = glob_operator<arma::sp_cx_mat, state_in, state_out>(op.in_basis(),op.out_basis());

    c_op.mat = arma::sp_cx_mat(op.mat, arma::zeros<arma::sp_mat>(arma::size(op.mat)));

    return c_op;

}

template<class state_in, class state_out>
glob_operator<arma::cx_mat, state_in, state_out>
to_complex(const glob_operator<arma::mat, state_in, state_out>& op){

    auto c_op = glob_operator<arma::cx_mat, state_in, state_out>(op.in_basis(),op.out_basis());

    c_op.mat = arma::cx_mat(op.mat, arma::zeros<arma::mat>(arma::size(op.mat)));

    return c_op;

}


template<class mat_t, class state_in, class state_middle, class state_out>
inline glob_operator<mat_t, state_in, state_out>
    operator*(const glob_operator<mat_t, state_middle, state_out>& op1, const glob_operator<mat_t, state_in, state_middle>& op2){
    
    if (op2.out_basis()!=op1.in_basis())
        throw incompatible_product();

    auto product = glob_operator<mat_t, state_in, state_out>(op2.in_basis(), op1.out_basis());
    product.mat = op1.mat * op2.mat;

    return product; 
}

template<class mat_t, class state_in, class state_out>
inline glob_operator<mat_t, state_in, state_out>
    operator+(glob_operator<mat_t, state_in, state_out> op1, const glob_operator<mat_t, state_in, state_out>& op2){

    op1+=op2;
    return op1;

}

template<class mat_t, class state_in, class state_out>
inline glob_operator<mat_t, state_in, state_out>
    operator-(glob_operator<mat_t, state_in, state_out> op1, const glob_operator<mat_t, state_in, state_out>& op2){

    op1-=op2;
    return op1;
}

template<class mat_t, class state_in, class state_out>
inline glob_operator<mat_t, state_in, state_out>
    operator*(Real alpha, glob_operator<mat_t, state_in, state_out> op){
    
    op *= alpha;
    return op;
}

template<class mat_t, class state_in, class state_out>
inline glob_operator<mat_t, state_in, state_out>
    operator*(Complex alpha, glob_operator<mat_t, state_in, state_out> op){
    
    op *= alpha;
    return op;
}



template <class sym_state>
inline glob_operator<arma::sp_cx_mat, product_state, sym_state> 
    projector(std::shared_ptr<basis<product_state>> product_basis, std::shared_ptr<basis<sym_state>> symmetric_basis)
{
    
    auto proj = glob_operator<arma::sp_cx_mat, product_state, sym_state>(product_basis, symmetric_basis);

    for (int i=0; i<symmetric_basis->dim(); i++){

        sym_state ss = (*symmetric_basis)[i];

        auto components = ss.components(*product_basis);

        for (component el : components){
            int j = el.index;
            auto coeff = el.overlap;

            proj.mat(i,j) = coeff;
        }

    }

    return proj;

}

template <class sym_state>
inline glob_operator<arma::sp_mat, product_state, sym_state> 
    real_projector(std::shared_ptr<basis<product_state>> product_basis, std::shared_ptr<basis<sym_state>> symmetric_basis)
{
    
    auto proj = glob_operator<arma::sp_mat, product_state, sym_state>(product_basis, symmetric_basis);

    for (int i=0; i<symmetric_basis->dim(); i++){

        sym_state ss = (*symmetric_basis)[i];

        auto components = ss.components(*product_basis);

        for (component el : components){
            int j = el.index;

            auto coeff = el.overlap.real();

            proj.mat(i,j) = coeff;
        }

    }

    return proj;

}



#endif