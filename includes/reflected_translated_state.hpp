#pragma once
#ifndef __REFLECTED_TRANSLATED_STATE_H__
#define __REFLECTED_TRANSLATED_STATE_H__


#include "symmetrized_state.hpp"
#include "translated_state.hpp"
#include "symmetries_implementation.hpp"


//class representing a state symmetrized under translation and reflection (parity)
class reflected_translated_state : public symmetrized_state<translated_state, reflection_symmetry>{

    public:
        
        reflected_translated_state(){}

        reflected_translated_state(const product_state& state_rep): symmetrized_state<translated_state, reflection_symmetry>(state_rep){}

        int reflection_period(){
            return symmetry_period();
        }

        int translation_period(){
            return state_.translation_period();
        }

};

#endif