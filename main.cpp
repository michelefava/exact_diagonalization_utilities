#include <iostream>
#include <cstdlib>
#include <armadillo>

#include "includes/product_state.hpp"
#include "includes/basis_build.hpp"
#include "includes/translated_state.hpp"
#include "includes/reflected_translated_state.hpp"
#include "includes/glob_operator.hpp"

int main(){
    int L = 5;
    int d = 2;
    product_state s = product_state(L, d);
    s.set_site(0, 0);
    s.set_site(1, 0);
    s.set_site(2, 0);
    s.set_site(3, 0);
    s.set_site(4, 0);


    L = 4;
    std::shared_ptr<sp_spin_1_2_basis> spin_basis = std::make_shared<sp_spin_1_2_basis>(sp_spin_1_2_basis(L));
    std::shared_ptr<dense_basis<translated_state>> trans_basis = 
        std::make_shared<dense_basis<translated_state>>(dense_basis<translated_state>(L,d,pow(2,L)/L*2));
    symmetrize_basis(*spin_basis, *trans_basis);

    auto P = projector<translated_state>(spin_basis, trans_basis);

    L = 3;
    int N = L;
    d = N+1;
    dense_basis<product_state> ds(L, d, binomial(2*L-1,L-1));
    build_full_U1_basis(ds,N);

    sp_U1_basis ss = sp_U1_basis(L,N);

}