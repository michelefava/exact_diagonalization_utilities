#include "../includes/product_state.hpp"
#include "../includes/symmetries_implementation.hpp"


void reflect_state(product_state& state){
   int L = state.L();
   for (int i=0; i<L/2; i++){
       auto tmp = state.get_site(L-i-1);
       state.set_site(L-i-1, state.get_site(i));
       state.set_site(i, tmp);
   }
}

void shift_state_to_right(product_state &state)
{
    int L = state.L();
    auto tmp = state.get_site(L - 1);
    for (int i = L - 1; i > 0; i--)
    {
        state.set_site(i, state.get_site(i - 1));
    }
    state.set_site(0, tmp);
}