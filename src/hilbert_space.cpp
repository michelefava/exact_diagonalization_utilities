#include "../includes/basis.hpp"
#include "../includes/product_state.hpp"

class non_empty_space{};

void recursive_basis_build(dense_basis<product_state>& space, product_state& state, const int site){

    if (site==-1){
        space.add_to_basis(state);
        return;
    }

    for (int local_state = 0; local_state<space.d(); local_state++){
        state.set_site(site,local_state);
        recursive_basis_build(space, state, site-1);
    }

}

void build_full_basis(dense_basis<product_state>& space){
    if (space.dim()!=0)
        throw non_empty_space();

    product_state state = product_state(space.L(),space.d());

    recursive_basis_build(space,state, space.L()-1);
    
}

void recursive_U1_basis_build(dense_basis<product_state>& space, product_state& state, const int site, const int N){

    if (site==state.L()){
        if (N == 0)
            space.add_to_basis(state);
        return;
    }

    for (int local_state = 0; local_state<space.d(); local_state++){
        if (N-local_state<0)
            break;
        state.set_site(site,local_state);
        recursive_U1_basis_build(space, state, site+1, N-local_state);
    }

}

void build_full_U1_basis(dense_basis<product_state>& space, const int N){
    if (space.dim()!=0)
        throw non_empty_space();

    if (space.d()<N+1){
        std::cout << "Warning: truncating the maximum occupation to " << space.d()-1 <<"\n";
    }

    product_state state = product_state(space.L(),space.d());

    recursive_U1_basis_build(space,state, 0, N);
    
}
