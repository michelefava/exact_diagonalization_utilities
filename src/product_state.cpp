#include "../includes/product_state.hpp"
#include <ostream>

bool operator==(const product_state& state1, const product_state& state2){
    if (state1.d()!=state2.d())
        throw mismatching_local_dimensions();
    
    if (state1.L()!=state2.L())
        throw mismatching_system_size();

    for (int i=0; i<state1.L(); i++){
        if (state1.get_site(i) != state2.get_site(i))
            return false;
    }
    return true;
}

bool operator!=(const product_state &state1, const product_state &state2){
    return !(state1==state2);
}

std::ostream& operator<< (std::ostream& os, const product_state& state){
    for (int i=0; i<state.L(); i++){
        os << state.get_site(i) <<" ";
    }
    os<< "\n";

    return os;
}
