LIB_FLAGS = -larmadillo  
#-lopenblas -llapack
#direct linking with armadillo, blas, lapack

OPT = -O3
## As the Armadillo library uses recursive templates, compilation times depend on the level of optimisation:
##
## -O0: quick compilation, but the resulting program will be slow
## -O1: good trade-off between compilation time and execution speed
## -O2: produces programs which have almost all possible speedups, but compilation takes longer
## -O3: enables auto vectorisation when using gcc

#EXTRA_OPT = -fwhole-program
## Uncomment the above line if you're compiling all source files into one program in a single hit

#DEBUG = -g
#Deccomment the above line to enable debugging, e.g. via gdb

#FINAL = -DARMA_NO_DEBUG
## Uncomment the above line to disable Armadillo's checks.
## Not recommended unless your code has been first thoroughly tested!

EXTRA_FLAG = -Wall

CXX_STD = -std=c++11


CXXFLAGS = $(CXX_STD) $(DEBUG) $(FINAL) $(OPT) $(EXTRA_OPT) $(EXTRA_FLAG)


OBJS =	main.o 

LIBS = includes/*.hpp

TEST_IMPL = test/*.cpp

SRC = src/*.cpp

TARGET = main

$(TARGET):	$(OBJS) $(LIBS) $(SRC)
	$(CXX) -o $(TARGET) $(OBJS) $(SRC) $(LIB_FLAGS)

test/main_test: $(LIBS) $(TEST_IMPL) $(SRC)
	$(CXX) -std=c++11 -o test/main_test $(TEST_IMPL) $(SRC) $(LIB_FLAGS)

all:	$(TARGET) test/main_test

test: test/main_test

clean:
	rm -f $(OBJS) $(TARGET)
	rm -f test/main_test