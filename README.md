This header-only library is meant to provide utilities to implement your ED code, working on a symmetrized subspace (w.r.t. translation and parity).

Unlike other libraries and programs, such as ALPS or diagHam, this library is not meant to take care of every aspect of the ED process. It is just meant to help you quickly write the matrices of the relevant operators in the symmetrized subspace. It's then up to you to perform the required algebraic operations on the symmetrized matrices.

The matrices in this library are dealt with using the Armadillo library (http://arma.sourceforge.net/)